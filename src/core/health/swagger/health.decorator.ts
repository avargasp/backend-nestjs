import { applyDecorators } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiServiceUnavailableResponse, ApiTags } from '@nestjs/swagger';
import { HealthResponseDto } from '../dtos/health.response.dto';

export const StatusSwagger = () => {
  return applyDecorators(
    ApiTags('Health'),
    ApiOperation({
      summary: 'Simple endpoint to check the app is alive',
      description: 'A more detailed explanation of what the api does',
    }),
    ApiOkResponse({
      type: HealthResponseDto,
    }),
  );
};

const checkOkResponse = {
  schema: {
    example: {
      status: 'ok',
      info: {
        staticContent: {
          status: 'up',
        },
      },
      error: {},
      details: {
        staticContent: {
          status: 'up',
        },
      },
    },
  },
};

const checkServiceUnavailableResponse = {
  description: 'Service Unavailable',
  schema: {
    example: {
      status: 'error',
      info: {},
      error: {
        staticContent: {
          status: 'down',
          message: 'Request failed with status code 400',
          statusCode: 400,
          statusText: 'Bad Request',
        },
      },
      details: {
        staticContent: {
          status: 'down',
          message: 'Request failed with status code 400',
          statusCode: 400,
          statusText: 'Bad Request',
        },
      },
    },
  },
};

export const CheckSwagger = () => {
  return applyDecorators(
    ApiTags('Health'),
    ApiOperation({
      summary: 'Checks if associated externals services are alive/available',
      description: 'A more detailed explanation of what the api does',
    }),
    ApiOkResponse(checkOkResponse),
    ApiServiceUnavailableResponse(checkServiceUnavailableResponse),
  );
};
