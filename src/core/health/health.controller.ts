import { Controller, Get } from '@nestjs/common';
import {
  HealthCheck,
  HealthCheckResult,
  HealthCheckService,
  HttpHealthIndicator,
  MikroOrmHealthIndicator,
} from '@nestjs/terminus';

import { HealthResponseDto } from './dtos/health.response.dto';
import { HealthService } from './health.service';
import { CheckSwagger, StatusSwagger } from './swagger/health.decorator';

@Controller('health')
export class HealthController {
  constructor(
    private readonly healthService: HealthService,
    private readonly healthCheckService: HealthCheckService,
    private readonly database: MikroOrmHealthIndicator,
    private readonly http: HttpHealthIndicator,
  ) {}

  @Get()
  @StatusSwagger()
  status(): HealthResponseDto {
    return this.healthService.getStatus();
  }

  @HealthCheck()
  @Get('check')
  @CheckSwagger()
  check(): Promise<HealthCheckResult> {
    // * Can't use configService on controller
    return this.healthCheckService.check([
      () => this.database.pingCheck('database', { timeout: 1500 }),
      () => this.http.pingCheck('pokeapi', process.env['POKEAPI_BASE_URL'] ?? ''),
    ]);
  }
}
