export enum Flag {
  Easy = 'easy',
  Jumbo = 'jumbo',
  Paris = 'paris',
  Sisa = 'sisa',
  Spid = 'spid',
  Wong = 'wong',
}
