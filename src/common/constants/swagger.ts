import type { HelpCenterHeaders } from 'common/types/hc-header.type';

export const dummyTraceId = '3cbd0525-72d7-4668-8349-5cc621108251';
export const dummyId = 1;
export const dummyFlag = 'paris';
export const dummyFlagId = 1;
export const dummyHeader: HelpCenterHeaders = {
  'trace-id': '1',
  'flag-id': 1,
  'flag-name': 'jumbo',
  'flag-country': 'CL',
};

export const responseErrorUnauthorized = {
  description: 'Unauthorized',
  schema: {
    example: {
      statusCode: 401,
      message: 'Unauthorized',
    },
  },
};
export const responseErrorBadRequest = {
  description: 'Bad Request',
  schema: {
    example: {
      statusCode: 400,
      message: 'Bad Request',
    },
  },
};
export const responseErrorInternalServer = {
  description: 'Internal Server Error',
  schema: {
    example: {
      statusCode: 500,
      message: 'Internal Server Error',
    },
  },
};
export const responseErrorServiceUnavailable = {
  description: 'Service Unavailable',
  schema: {
    example: {
      statusCode: 503,
      message: 'Service Unavailable',
    },
  },
};
