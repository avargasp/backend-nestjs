import { Type, applyDecorators } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiHeaders,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiOperation,
  ApiSecurity,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import {
  dummyTraceId,
  responseErrorBadRequest,
  responseErrorInternalServer,
  responseErrorUnauthorized,
} from 'common/constants/swagger';
import { Country } from '../enums/countries';
import { Flag } from '../enums/flags';

export const Headers = () => {
  return applyDecorators(
    ApiHeaders([
      {
        name: 'flag-name',
        description: 'Requested flag name',
        enum: Flag,
        example: 'paris',
        required: true,
      },
      {
        name: 'flag-country',
        description: 'Requested country name',
        enum: Country,
        example: 'cl',
        required: true,
      },
      {
        name: 'trace-id',
        description: 'Unique identifier for tracing requests',
        example: dummyTraceId,
        required: false,
      },
    ]),
  );
};

export const ResponseError = () => {
  return applyDecorators(
    ApiUnauthorizedResponse(responseErrorUnauthorized),
    ApiBadRequestResponse(responseErrorBadRequest),
    ApiInternalServerErrorResponse(responseErrorInternalServer),
  );
};

export const ApplyCommonApiDecorators = <TDto extends Type<unknown>>(
  summary: string,
  descriptionOperation: string,
  descriptionOkResponse: string,
  isArray: boolean,
  dto: TDto,
) => {
  return applyDecorators(
    ApiSecurity('api-key', ['api-key']),
    Headers(),
    ApiOperation({
      summary,
      description: descriptionOperation,
    }),
    ApiOkResponse({
      description: descriptionOkResponse,
      isArray,
      type: dto,
    }),
    ResponseError(),
  );
};
