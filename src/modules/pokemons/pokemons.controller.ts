import { Controller, Get, Param, UseGuards, UseInterceptors } from '@nestjs/common';
import type { PaginatedListResponseDto } from 'common/dtos/paginated-list.response.dto';
import { ApiKeyAuthGuard } from 'core/auth/guards/key-auth.guard';
import { HeadersInterceptor } from 'core/interceptors/headers.interceptor';
import type { PokemonResponseDto } from './dto/pokemon.response.dto';
import { PokemonsService } from './pokemons.service';
import { PokemonFindAll, PokemonFindOne } from './swagger/pokemon.decorator';

@UseGuards(ApiKeyAuthGuard)
@UseInterceptors(HeadersInterceptor)
@Controller('pokemons')
export class PokemonsController {
  constructor(private readonly pokemonsService: PokemonsService) {}

  @Get()
  @PokemonFindAll()
  findAll(@Param('limit') limit?: number, @Param('offset') offset?: number): Promise<PaginatedListResponseDto> {
    return this.pokemonsService.findAll(limit, offset);
  }

  @Get(':name')
  @PokemonFindOne()
  findOne(@Param('name') name: string): Promise<PokemonResponseDto> {
    return this.pokemonsService.findOne(name);
  }
}
