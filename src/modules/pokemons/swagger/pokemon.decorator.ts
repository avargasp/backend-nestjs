import { applyDecorators } from '@nestjs/common';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { PaginatedListResponseDto } from 'common/dtos/paginated-list.response.dto';
import { ApplyCommonApiDecorators } from 'common/swagger/common.decorator';
import { PokemonResponseDto } from 'modules/pokemons/dto/pokemon.response.dto';

export const PokemonFindAll = () => {
  return applyDecorators(
    ApiTags('Pokemon'),
    ApplyCommonApiDecorators(
      'Get pokemons by flag',
      'API to obtain pokemons by flag and country',
      'An array of pokemons',
      true,
      PaginatedListResponseDto,
    ),
    ApiParam({
      name: 'limit',
      required: false,
      description: 'paginated list limit',
    }),
    ApiParam({
      name: 'offset',
      required: false,
      description: 'paginated list offset',
    }),
  );
};

export const PokemonFindOne = () => {
  return applyDecorators(
    ApiTags('Pokemon'),
    ApplyCommonApiDecorators(
      'Get one pokemon by flag',
      'API to obtain one pokemon by flag and country',
      'One pokemon',
      false,
      PokemonResponseDto,
    ),
    ApiParam({
      name: 'name',
      description: 'Wanted pokemon exact name',
    }),
  );
};
