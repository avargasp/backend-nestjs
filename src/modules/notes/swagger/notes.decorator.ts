import { applyDecorators } from '@nestjs/common';
import { ApplyCommonApiDecorators, Headers, ResponseError } from 'common/swagger/common.decorator';
import { ApiBody, ApiCreatedResponse, ApiNoContentResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { PaginatedListResponseDto } from 'common/dtos/paginated-list.response.dto';
import { Note } from 'modules/notes/entities/note.entity';
import { UpdateNoteDto } from 'modules/notes/dto/update-note.dto';
import { CreateNoteDto } from 'modules/notes/dto/create-note.dto';

export const NotesFindAll = () => {
  return applyDecorators(
    ApiTags('Note'),
    ApplyCommonApiDecorators(
      'Get notes list',
      'API to obtain all notes by flag and country',
      'An array of notes',
      true,
      PaginatedListResponseDto,
    ),
    ApiParam({
      name: 'limit',
      required: false,
      description: 'paginated list limit',
    }),
    ApiParam({
      name: 'offset',
      required: false,
      description: 'paginated list offset',
    }),
  );
};

export const NotesFindOne = () => {
  return applyDecorators(
    ApiTags('Note'),
    ApplyCommonApiDecorators('Get one note', 'API to obtain one note by flag and country', 'One note', false, Note),
    ApiParam({
      name: 'id',
      description: 'Wanted note id',
    }),
  );
};

export const NotesCreate = () => {
  return applyDecorators(
    ApiTags('Note'),
    Headers(),
    ApiOperation({
      summary: 'Create one note',
      description: 'API to create one note by flag and country',
    }),
    ApiCreatedResponse({
      description: 'Created note',
      isArray: false,
      type: Note,
    }),
    ResponseError(),
    ApiBody({
      type: CreateNoteDto,
      description: 'Note structure to create',
    }),
  );
};

export const NotesUpdate = () => {
  return applyDecorators(
    ApiTags('Note'),
    ApplyCommonApiDecorators('Update one note', 'API to update one note by flag and country', 'One note', false, Note),
    ApiParam({
      name: 'id',
      description: 'Note ID to update',
    }),
    ApiBody({
      type: UpdateNoteDto,
      description: 'Note structure to update',
    }),
  );
};

export const NotesRemove = () => {
  return applyDecorators(
    ApiTags('Note'),
    Headers(),
    ApiOperation({
      summary: 'Delete one note',
      description: 'API to delete one note by flag and country',
    }),
    ApiNoContentResponse({
      description: 'Deleted note',
      isArray: false,
      type: Note,
    }),
    ResponseError(),
    ApiParam({
      name: 'id',
      description: 'Note ID to delete',
    }),
  );
};
