import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import type { PaginatedListResponseDto } from 'common/dtos/paginated-list.response.dto';
import { ApiKeyAuthGuard } from 'core/auth/guards/key-auth.guard';
import { HeadersInterceptor } from 'core/interceptors/headers.interceptor';
import type { CreateNoteDto } from './dto/create-note.dto';
import type { UpdateNoteDto } from './dto/update-note.dto';
import type { Note } from './entities/note.entity';
import { NotesService } from './notes.service';
import { NotesCreate, NotesFindAll, NotesFindOne, NotesUpdate, NotesRemove } from './swagger/notes.decorator';

@UseGuards(ApiKeyAuthGuard)
@UseInterceptors(HeadersInterceptor)
@Controller('notes')
export class NotesController {
  constructor(private readonly notesService: NotesService) {}

  @Get()
  @NotesFindAll()
  findAll(@Param('limit') limit?: number, @Param('offset') offset?: number): Promise<PaginatedListResponseDto> {
    return this.notesService.findAllByFlag(limit, offset);
  }

  @Get(':id')
  @NotesFindOne()
  findOne(@Param('id', ParseIntPipe) id: number): Promise<Note> {
    return this.notesService.findOne(id);
  }

  @Post()
  @NotesCreate()
  @HttpCode(HttpStatus.CREATED)
  create(@Body() createNoteDto: CreateNoteDto): Promise<Note> {
    return this.notesService.create(createNoteDto);
  }

  @Patch(':id')
  @NotesUpdate()
  update(@Param('id', ParseIntPipe) id: number, @Body() updateNoteDto: UpdateNoteDto): Promise<Note> {
    return this.notesService.update(id, updateNoteDto);
  }

  @Delete(':id')
  @NotesRemove()
  @HttpCode(HttpStatus.NO_CONTENT)
  remove(@Param('id') id: number): Promise<void> {
    return this.notesService.remove(id);
  }
}
